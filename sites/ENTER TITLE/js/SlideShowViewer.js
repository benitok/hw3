/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var i = 0;
var playing = false;
var timedFunction;

$(document).ready(function() {
     $.getJSON("js/SlideShow.json", function(ss) {
     document.getElementById("good").innerHTML = ss.title;
     document.getElementById("slide").src = "img/" + ss.slides[0].image_file_name;
     document.getElementById("caption").innerText = ss.slides[0].caption;
     $("#prev").prop("disabled", true);
     if(ss.slides.length <= 1)
         $("#next").prop("disabled", true);
     $("#next").click( function() {
        document.getElementById("slide").src = "img/" + ss.slides[++i].image_file_name;
        document.getElementById("caption").innerText = ss.slides[i].caption;
        $("#prev").prop("disabled", false);
        if(i == ss.slides.length - 1) 
            $("#next").prop("disabled", true);
     });
     $("#prev").click( function() {
         document.getElementById("slide").src = "img/" + ss.slides[--i].image_file_name;
         document.getElementById("caption").innerText = ss.slides[i].caption;
         $("#next").prop("disabled", false);
        if(i == 0) 
            $("#prev").prop("disabled", true);
     });
     $("#play").click( function() {
         if(!playing) {
            $("#next").prop("disabled", true);
            $("#prev").prop("disabled", true);
            document.getElementById("playImage").src = "../../images/icons/PauseButton.png";
            playing = true;
            setTimeOut(fadeOutSlide, 500);
            timedFadeOut = setInterval(fadeOutSlide, 3000);
            timedFunction = setInterval(playSlideShow, 3000);
        }
        else {
            document.getElementById("playImage").src = "../../images/icons/play_button.svg";
            playing = false;
            $("#next").prop("disabled", i == ss.slides.length - 1);
            $("#prev").prop("disabled", i == 0);
            clearInterval(timedFunction);
        }
     });
     function playSlideShow() {
        if(i == ss.slides.length - 1) 
            i = -1;
        document.getElementById("slide").src = "img/" + ss.slides[++i].image_file_name;
        $("#slide").fadeIn(500);
        document.getElementById("caption").innerText = ss.slides[i].caption;
     }
     });
     function fadeOutSlide() {
         $("#slide").fadeOut(500);
     };
});



//document.getElementById("good").innerText = "good";

//var json = JSON.parse("js/SlideShow");

/*$.getJSON('SlideShow.json', function(data) {
    document.getElementById("good").innerText = data.title;
});*/

//document.getElementById("good").innerText = json.title;

  /*      var reader = new FileReader();
        reader.onload = function(e) {
            document.getElementById("jsonDisplay").innerText = reader.result;
        };
        reader.readAsText("./js/SlideShow.json");*/
/*$(document).ready(function() {
    alert("Great");
});*/

   /* if(window.FileReader) {
    }
    else {
        alert("No file reader :(");
    }
    alert("Hello");
    window.onload = function() {
        alert("Test alert");
        var jsonDisplay = document.getElementById('jsonDisplay');
        var bye = document.getElementById('bye');
        var reader = new FileReader();
        reader.onload = function(e) {
            bye.innerText = reader.result;
        };
        reader.readAsText("./js/SlideShow.json");
        document.getElementById('good').innerText = "good";
    };*/
    


